package com.learn.kafka.palmetto.order.listener;

import com.learn.kafka.entity.dto.OrderDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class OrderTopicListenerTest {

    @InjectMocks
    private OrderTopicListener orderTopicListener;

    @Mock
    private KafkaTemplate<Long, OrderDTO> kafkaTemplateMock;

    @Mock
    private OrderDTO orderDTOMock;

    @Test
    public void processIncomeOrderTest() throws InterruptedException {
        String notificationTopicName = "notification";
        int clientPartition = 0;
        int courierPartition = 1;
        long orderId = 1L;

        orderTopicListener.processIncomeOrder(orderDTOMock, orderId);

        verify(kafkaTemplateMock).send(notificationTopicName, clientPartition, orderId, orderDTOMock);
        verify(kafkaTemplateMock).send(notificationTopicName, courierPartition, orderId, orderDTOMock);
    }
}