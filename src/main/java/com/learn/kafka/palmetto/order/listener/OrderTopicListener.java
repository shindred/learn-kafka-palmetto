package com.learn.kafka.palmetto.order.listener;

import com.learn.kafka.entity.dto.OrderDTO;
import com.learn.kafka.entity.enumaration.OrderStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OrderTopicListener {

    private static final String NOTIFICATION_TOPIC_NAME = "notification";
    private static final int CLIENT_PARTITION = 0;
    private static final int COURIER_PARTITION = 1;

    private final KafkaTemplate<Long, OrderDTO> orderDTOKafkaTemplate;

    @KafkaListener(topics = "order")
    public void processIncomeOrder(@Payload OrderDTO orderDTO, @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) Long id) throws InterruptedException {
        Thread.sleep(5000);
        orderDTO.setOrderStatus(OrderStatus.READY_FOR_DELIVERY);
        sendNotification(CLIENT_PARTITION, id, orderDTO);
        sendNotification(COURIER_PARTITION, id, orderDTO);
    }

    private void sendNotification(int partition, long id, OrderDTO orderDTO) {
        orderDTOKafkaTemplate.send(NOTIFICATION_TOPIC_NAME, partition, id, orderDTO);
    }
}
