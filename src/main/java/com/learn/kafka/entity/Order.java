package com.learn.kafka.entity;

import com.learn.kafka.entity.enumaration.OrderStatus;
import lombok.Data;

@Data
public class Order {

    private Long id;
    private String name;
    private OrderStatus orderStatus;
    private Receiver receiver;
}
